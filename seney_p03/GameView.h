//
//  GameView.h
//  seney_p03
//
//  Created by Nicholas Ryan Seney on 2/19/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"
@interface GameView : UIView


@property (nonatomic,strong) Jumper *jumper;
@property (nonatomic,strong) NSMutableArray *bricks;
@property float tilt;
-(void)animationStuff:(CADisplayLink *)sender;
@property float prevDy;
@property (strong, nonatomic) IBOutlet UILabel *highScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *bounceLabel;
@property (strong, nonatomic) IBOutlet UILabel *gameOverLabel;
@property (strong,nonatomic) IBOutlet UIButton *resetButton;


@end
