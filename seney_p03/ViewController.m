//
//  ViewController.m
//  seney_p03
//
//  Created by Nicholas Ryan Seney on 2/19/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) CADisplayLink *displayLink;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(animationStuff:)];
    [_displayLink setPreferredFramesPerSecond:30];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) changeTilt:(id) sender
{
  //  NSLog(@"I changed value");
    UISlider* tempSlide = sender;
    [_gameView setTilt:([tempSlide value])];
}


@end
