//
//  GameView.m
//  seney_p03
//
//  Created by Nicholas Ryan Seney on 2/19/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize jumper,bricks;
@synthesize tilt,prevDy;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
//Need to fix these
bool pastfirstLevel = false;
int bounces = 10;
int colorCounter = 0;
int highScore;
float multiplie=0;

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        CGRect bounds = [self bounds];
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 20, 20)];
        [jumper setBackgroundColor:[UIColor redColor]];
        [jumper setDx:0];
        [jumper setDy:10];
        [self addSubview:jumper];
        prevDy = [jumper dy];
    }
    [self makeBricks:nil];
    [super setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"oneedit.png"]]];
    return self;
}

-(IBAction) makeBricks:(id)sender
{
    multiplie = 0;
    if([_gameOverLabel.text isEqualToString:(@"Game Over!")])
    {
        [jumper setBackgroundColor:[UIColor redColor]];
        [jumper setDx:0];
        [jumper setDy:10];
        _gameOverLabel.text = @"";
    }
    if(pastfirstLevel)
    {
        [super setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"oneedit.png"]]];
        pastfirstLevel = false;
    }
    CGRect bounds = [self bounds];
    float height = 20;
    float width = (bounds.size.width) * .2;
    if(sender == _resetButton)
    {
        [jumper setDx:0];
        [jumper setDy:10];
        [jumper setBackgroundColor:[UIColor redColor]];
        CGPoint p = CGPointMake(bounds.size.width/2, bounds.size.height - 20);
        [jumper setCenter:p];
        highScore = 0;
        bounces = 10;
    }
    
    
    
    
    [_highScoreLabel setText:([NSString stringWithFormat:@"%d",highScore])];
    [_bounceLabel setText:([NSString stringWithFormat:@"Lives:%d",bounces])];
    if(bricks)
    {
        for(Brick* tBrick in bricks)
        {
            [tBrick removeFromSuperview];
        }
    }
    bricks = [[NSMutableArray alloc] init];
    Brick *temp;
    for(int i=0;i<7;i++)
    {
        // NSLog(@"i=%d",i);
        
        temp = [[Brick alloc] initWithFrame:CGRectMake(0,0,width,height)];
        [temp setBackgroundColor:[UIColor blueColor]];
        if(rand()%11 == 1)
        {
            if(rand()%2 ==0)
            {
                [temp setBackgroundColor:[UIColor redColor]];
            }
            else
            {
                [temp setBackgroundColor:[UIColor greenColor]];
            }
        }
        [self addSubview:temp];
        if(i==6)
        {
            [temp setCenter:(CGPointMake(arc4random()%(int)(bounds.size.width*.80),(bounds.size.height - arc4random()%50)))];
        }
        else
            [temp setCenter:(CGPointMake(arc4random()%(int)(bounds.size.width*.80),arc4random()%(int)(bounds.size.height*.85)))];
        [bricks addObject:(temp)];
    }
    
}


-(void)gameOver:(id)sender
{
    _gameOverLabel.text = @"Game Over!";
    for(Brick* tBrick in bricks)
    {
        [tBrick removeFromSuperview];
    }
}

-(void)animationStuff:(CADisplayLink *)sender
{
    
    // NSLog(@"Enter here");
    CFTimeInterval ts = [sender timestamp];
    CGRect bounds = [self bounds];
    if([jumper dy] < highScore%(int)(bounds.size.height*-1))
    {
        highScore += [jumper dy] * -1;
    }
    UIColor *mycolor;
    [jumper setDy:([jumper dy] + .3)];
    [jumper setDx:(jumper.dx =tilt)];
    CGPoint p = ([jumper center]);
    p.x += [jumper dx];
    if(multiplie == 0)
        p.y += [jumper dy];
    if(p.y > bounds.size.height)
    {
        multiplie = 0;
        if(pastfirstLevel)
        {
            if(bounces == 0)
            {
                [self gameOver:nil];
            }
            else{
                jumper.dy = -10;
                p.y = bounds.size.height;
                bounces --;
            }
            
        }
        else{
            jumper.dy = -10;
            p.y = bounds.size.height;
            
        }
        
    }
    else if(p.y < 0)
    {
        multiplie = 0;
        p.y = bounds.size.height+p.y;
        [self makeBricks:nil];
        [self changeColors:nil];
        pastfirstLevel = true;
    }
    if(p.x > bounds.size.width)
    {
        p.x = 5;
    }
    else if(p.x < 0)
    {
        p.x = bounds.size.width - 5;
        
    }
    for(Brick* tbricks in bricks)
    {
        CGRect b = [tbricks frame];
        mycolor = [tbricks backgroundColor];
        if(p.y > b.origin.y -5 && p.y < b.origin.y + b.size.height/2)
        {
            if(p.x > b.origin.x && p.x < b.origin.x + (b.size.width) && multiplie < 5)
            {
                //NSLog(@"multi=%f",multiplie);
                multiplie += .02;
                p.y = b.origin.y;
                break;
                
            }
            else if(p.x > b.origin.x -10 && p.x < b.origin.x + b.size.width + 10)
            {
                // NSLog(@"multi=%f",multiplie);
                if(mycolor == [UIColor redColor])
                {
                    
                }
                else if(mycolor == [UIColor greenColor])
                {
                    jumper.dy = -13 - multiplie;
                    multiplie = 0;
                    p.y = b.origin.y;
                    [tbricks setBackgroundColor:[UIColor redColor]];
                }
                else
                {
                    if(rand()%4 == 0)
                        [tbricks setBackgroundColor:[UIColor redColor]];
                    else if(rand()%5 == 1)
                        [tbricks setBackgroundColor:[UIColor greenColor]];
                    jumper.dy = -10 - multiplie;
                    multiplie = 0;
                    p.y = b.origin.y;
                }
            }
            else
            {
                multiplie = 0;
            }
        }
        else if(p.y > b.origin.y + b.size.height*.75 && p.y < b.origin.y + b.size.height)
        {
            if(p.x > b.origin.x && p.x < b.origin.x + (b.size.width))
            {
                // NSLog(@"multi=%f",multiplie);
                jumper.dy = 0;
                p.y = b.origin.y + b.size.height;
            }
        }
    }
    // NSLog(@"Bounds = %f",p.x);
    if(highScore > [_highScoreLabel.text integerValue])
        [_highScoreLabel setText:([NSString stringWithFormat:@"%d",highScore])];
    [_bounceLabel setText:([NSString stringWithFormat:@"Lives:%d",bounces])];
    [jumper setCenter:p];
    // Make frame local variable that keeps track of how many frames ive passed and multiply that
    
    
}

-(void)changeColors:(id)sender
{
    if(colorCounter%4==0)
    {
        [jumper setBackgroundColor:[UIColor yellowColor]];
        [super setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"two.png"]]];
    }
    else if(colorCounter%4 == 1)
    {
        [jumper setBackgroundColor:[UIColor whiteColor]];
        [super setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"three.png"]]];
    }
    else if(colorCounter%4 == 2)
    {
        [jumper setBackgroundColor:[UIColor blackColor]];
        [super setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"four.png"]]];
    }
    else if(colorCounter%4 == 3)
    {
        [jumper setBackgroundColor:[UIColor orangeColor]];
        [super setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"five.png"]]];
    }
    colorCounter++;
}

@end
