//
//  main.m
//  seney_p03
//
//  Created by Nicholas Ryan Seney on 2/19/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
